package com.jeff.flow.view.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jeff.flow.samples.BasicSample
import com.log.JFLog
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

// https://developer.android.com/topic/libraries/architecture/coroutines

@ExperimentalCoroutinesApi
class MainViewModel : ViewModel() {

    fun fun1() {
        JFLog.w("Foo1 Start")
        BasicSample.foo1().forEach { value ->
            JFLog.d("Foo1: $value")
        }
        JFLog.w("Foo1 End")
    }

    fun fun2() {
        JFLog.w("Foo2 Start")
        BasicSample.foo2().forEach { value ->
            JFLog.d("Foo2: $value")
        }
        JFLog.w("Foo2 End")
    }

    fun fun3() {
        JFLog.w("Foo3 Start")
        viewModelScope.launch {
            BasicSample.foo3().forEach { value ->
                JFLog.d("Foo3: $value")
            }
        }
        JFLog.w("Foo3 End")
    }

    fun fun4_1() {
        JFLog.w("Foo4-1 Start (Like sequence)")
        viewModelScope.launch {
            // 啟動協程以驗證主線程並未阻塞
            launch {
                for (k in 1..3) {
                    JFLog.i("I'm not blocked $k")
                    delay(500)
                }
            }

            // collect
            BasicSample.foo4().collect { value ->
                JFLog.d("Foo4-1: $value")
            }
        }
        JFLog.w("Foo4-1 End")
    }

    fun fun4_2() {
        JFLog.w("Foo4-2 Start")
        viewModelScope.launch {
            JFLog.i("Calling collect 1")
            BasicSample.foo4().collect { value ->
                JFLog.d("Foo4-2: $value")
            }

            JFLog.i("Calling collect 2")
            BasicSample.foo4().collect { value ->
                JFLog.d("Foo4-2: $value")
            }
        }
        JFLog.w("Foo4-2 End")
    }

    fun fun4_cancel() {
        JFLog.w("Foo4-Cancel Start")
        viewModelScope.launch {
            // Cancel when timeout
            withTimeoutOrNull(1000) {
                try {
                    BasicSample.foo4().collect { value ->
                        JFLog.d("Foo4-Cancel: $value")
                    }
                } catch (e: Exception) {
                    JFLog.e(e)
                }
            }
        }
        JFLog.w("Foo4-Cancel End")
    }

    fun fun5_cancel() {
        JFLog.w("Foo5-Cancel Start")
        viewModelScope.launch {
            // Cancel when timeout
            withTimeoutOrNull(1000) {
                /*
                try {
                    BasicSample.foo5().collect { value ->
                        JFLog.d("Foo5-Cancel(1): $value")
                    }
                } catch (e: Exception) {
                    JFLog.e(e)
                }
                */

                BasicSample.foo5()
                    .handleErrors()
                    .collect { value ->
                        JFLog.d("Foo5-Cancel(2): $value")
                    }
            }
        }
        JFLog.w("Foo5-Cancel End")
    }

    fun fun6() {
        viewModelScope.launch {
            flow {
                listOf("A", "B", "C", "D", "E").forEach { send ->
                    emit(send)
                }
            }
                // Default: on main thread
                .flowOn(Dispatchers.IO)
                .onCompletion { emit("Finish") }
                .collect {
                    JFLog.d(it)
                }
        }
    }

    // 統一處理Flow產生的錯誤
    private fun <T> Flow<T>.handleErrors(): Flow<T> = flow {
        try {
            collect { value -> emit(value) }
        } catch (e: Throwable) {
            JFLog.e(e)
        }
    }

    // 'map' 轉換資料
    // 'transform' 轉換發送物件, 可以收到後不往下面發送.
    // map放在onCompletion後, 可以影響onCompletion結果.
    fun fun7() {
        viewModelScope.launch {
            flow {
                listOf("A", "B", "C", "D", "E", "F", "G").forEach { send ->
                    emit(send)
                }
            }
                // Default: on main thread
                .flowOn(Dispatchers.IO)
                .map {
                    when (it) {
                        "C" -> "'map' by Jeff"
                        else -> it
                    }
                }
                .transform {
                    when (it) {
                        "A" -> JFLog.i("Skip 'A'")
                        "G" -> emit("'transform' to Last")
                        else -> emit(it)
                    }
                }
                .onCompletion { emit("Finish") }
                .collect {
                    JFLog.d(it)
                }
        }
    }

    // retry
    fun fun8_retry() {
        viewModelScope.launch {
            flow {
                for (i in (0..8).reversed()) {
                    if (i % 3 == 0) {
                        throw RuntimeException("Jeff $i")
                    } else {
                        emit(i)
                    }
                }
            }
                .retry { cause ->
                    JFLog.w("Cause: ${cause.message}")
                    false
                }
                .catch {
                    JFLog.e("Catch: ${it.message}")
                }
                .collect {
                    JFLog.d("Collect: $it")
                }
        }
    }

    // retryWhen (有計數)
    fun fun8_retryWhen() {
        viewModelScope.launch {
            flow {
                for (i in (0..8).reversed()) {
                    if (i % 3 == 0) {
                        throw RuntimeException("Jeff $i")
                    } else {
                        emit(i)
                    }
                }
            }
                .retryWhen { cause, attempt ->
                    JFLog.w("Cause: ${cause.message}, Attempt: $attempt")
                    attempt < 5L
                }
                .catch {
                    JFLog.e("Catch: ${it.message}")
                }
                .collect {
                    JFLog.d("Collect: $it")
                }
        }
    }
}