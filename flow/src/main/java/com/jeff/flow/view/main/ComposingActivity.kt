package com.jeff.flow.view.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.jeff.common.LinearActivity
import com.jeff.common.printCurrentThread
import com.log.JFLog
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

// 參考 https://medium.com/@picapro33/%E4%BD%BF%E7%94%A8-kotlin-coroutines-%E9%96%8B%E7%99%BC-android-app-%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B-77c1b7243c6a

class ComposingActivity : LinearActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    title = this::class.java.simpleName

    val zipBtn = initBtn("Zip")
    val combineBtn = initBtn("Combine")
    val mergeBtn = initBtn("Merge")

    val listener = View.OnClickListener { v ->
      lifecycleScope.launch {
        val flow1 = flow<String> {
          JFLog.d("Start-1")
          delay(6000L)
          JFLog.d("End-1")
          emit("1")
        }

        val flow2 = flow<String> {
          JFLog.d("Start-2")
          delay(4000L)
          JFLog.d("End-2")
          emit("2")
        }

        val flow3 = flow<String> {
          JFLog.d("Start-3")
          delay(2000L)
          JFLog.d("End-3")
          emit("3")
        }

        when (v) {
          zipBtn -> {
            flow1.zip(flow2) { r1, r2 ->
              r1 + r2
            }.zip(flow3) { r1, r2 ->
              r1 + r2
            }.collect {
              JFLog.w(it)
            }
          }

          mergeBtn -> {
            merge(flow1, flow2, flow3).collect {
              JFLog.w(it)
            }
          }
        }
      }
    }

    zipBtn.setOnClickListener(listener)
    mergeBtn.setOnClickListener(listener)
    combineBtn.setOnClickListener {
      lifecycleScope.launch {
        val numberFlow =
          (1..3).asFlow().onEach { delay(100) } // 發射數字 1..3，間隔 100 毫秒
        val stringFlow =
          flowOf("one", "two", "three").onEach { delay(400) } // 每 400 毫秒發射一次字符串
        val startTime = System.currentTimeMillis() // 記錄開始的時間
        numberFlow // 使用“combine”組合單個字符串
          .combine(stringFlow) { num, str ->
            "$num -> $str"
          }
          .collect { value ->
            JFLog.d("$value at ${System.currentTimeMillis() - startTime} ms from start")
          }
      }
    }

    initBtn("Zip Adv.") {
      lifecycleScope.launch {
        val job1 = async {
          delay(3000L)
          "A"
        }

        val job2 = async {
          delay(1000L)
          "B"
        }

        val job3 = async {
          delay(5000L)
          "C"
        }

        printCurrentThread(hierarchy = 3)
        JFLog.w("Wait Result")
        val result = job1.await() + job2.await() + job3.await()
        JFLog.w("$result")
      }
    }
  }
}