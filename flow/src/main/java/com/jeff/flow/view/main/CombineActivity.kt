package com.jeff.flow.view.main

import android.os.Bundle
import com.jeff.common.LinearActivity
import com.jeff.common.printCurrentThread
import com.log.JFLog
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flow
import kotlin.coroutines.CoroutineContext

class CombineActivity : LinearActivity(), CoroutineScope {

  private val job by lazy { SupervisorJob() }
  override val coroutineContext: CoroutineContext
    get() = job + Dispatchers.Main

  data class CombineObj(val first: String, val second: Float, val third: Int)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    title = this::class.java.simpleName

    initBtn("Combine") {
      val a = flow {
        repeat(3) {
          emit("A$it")
          delay(100)
        }
      }
      val b = flow {
        emit(0f)
        repeat(3) {
          delay(2000)
          emit(.1f + it)
        }
      }
      val c = flow {
        delay(400)
        emit(400)
      }

      launch {
        // 等同於 combineLatest
        combine(listOf(a, b, c)) {
          CombineObj(it[0] as String, it[1] as Float, it[2] as Int)
        }.collect {
          printCurrentThread(hierarchy = 3)
          JFLog.d("$it")
        }
      }
    }

    initBtn("Cancel children") {
      // cancelChildren 可以繼續使用Scope
      job.cancelChildren()
    }

    initBtn("Print default") {
      launch {
        // Check user interface thread
        JFLog.d("${mainLooper.isCurrentThread}")
      }
    }

    initBtn("Print io") {
      launch(Dispatchers.IO) {
        // Check user interface thread
        JFLog.d("${mainLooper.isCurrentThread}")
      }
    }

    initBtn("Print main") {
      launch(Dispatchers.Main) {
        // Check user interface thread
        JFLog.d("${mainLooper.isCurrentThread}")
      }
    }
  }

  override fun onDestroy() {
    super.onDestroy()
    // cancel 無法繼續使用Scope
    job.cancel("onDestroy")
  }
}