package com.jeff.flow.view.main

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.jeff.common.LinearActivity
import com.log.JFLog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

// 原文 https://kotlinlang.org/docs/reference/coroutines/flow.html
// 中文 https://www.kotlincn.net/docs/reference/coroutines/flow.html

class FlatteningFlowsActivity : LinearActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    title = this::class.java.simpleName

    initBtn("Btn1") {
      lifecycleScope.launch {
        useFlow()
          .flowOn(Dispatchers.IO)
          .catch { e ->
            JFLog.e(e)
          }
          .onCompletion { e ->
            if (e != null) {
              JFLog.e(e)
            } else {
              JFLog.d("useFlow onCompletion")
            }
          }
          .collect {
            JFLog.d(it)
          }
      }
    }

    // Contact依序執行.
    initBtn("Contact") {
      lifecycleScope.launch {
        (1..3).asFlow().onEach { delay(100) } // a number every 100 ms
          .map {
            JFLog.i("Send: $it")
            it
          }
          .flatMapConcat { testFlatteningFlows(it) }
          .collect { value ->
            // collect and print
            JFLog.d(value)
          }
      }
    }

    // Merge並行執行.
    initBtn("Merge") {
      lifecycleScope.launch {
        (1..3).asFlow().onEach { delay(100) } // a number every 100 ms
          .map {
            JFLog.i("Send: $it")
            it
          }
          .flatMapMerge { testFlatteningFlows(it) }
          .collect { value ->
            // collect and print
            JFLog.d(value)
          }
      }
    }

    // 捨棄前面只保留最後一筆Block
    initBtn("Latest") {
      lifecycleScope.launch {
        (1..3).asFlow().onEach { delay(100) } // a number every 100 ms
          .map {
            JFLog.i("Send: $it")
            it
          }
          .flatMapLatest { testFlatteningFlows(it) }
          .collect { value ->
            // collect and print
            JFLog.d(value)
          }
      }
    }

    // Sample: 在收到第一筆資料後執行期間, 在周期內最後一筆資料.
    // Result: 2-2, 4-2.
    initBtn("Sample") {
      flow {
        // @0
        emit("1-1")
        emit("1-2")
        delay(400L)
        // @400
        emit("2-1")
        emit("2-2")
        delay(1200L)
        // @1600
        emit("3-1")
        emit("3-2")
        delay(200L)
        // @1800
        emit("4-1")
        emit("4-2")

        delay(5000L) // 延長sample, 比較好觀察.
      }
        .sample(500).let {
          lifecycleScope.launch {
            it.collect { value ->
              // collect and print
              JFLog.d("$value")
            }
          }
        }
    }

    // debounce: 在接收到一筆資料開始算, 必須在timeout前沒有接收到新資料才會處理.
    // Result: 5
    initBtn("Debounce") {
      flow {
        emit(1)
        delay(1000)
        emit(2)
        delay(1000)
        emit(3)
        delay(1000)
        emit(4)
        delay(1000)
        emit(5)
      }
        .debounce(1100).let {
          lifecycleScope.launch {
            it.collect { value ->
              // collect and print
              JFLog.d("$value")
            }
          }
        }
    }
  }

  suspend fun justCoroutine() = withContext(Dispatchers.IO) {
    JFLog.d("Start justCoroutine")
    delay(2000L)
    JFLog.d("End justCorotuine")
  }

  // 建議用法
  private fun useFlow() = flow {
    emit("Start useFlow")
    delay(2000L)
    emit("End useFlow")
  }

  private fun testFlatteningFlows(input: Int) = flow {
    "Start No.$input".also {
      JFLog.w(it)
      emit(it)
    }

    delay(2000L)

    "End No.$input".also {
      JFLog.w(it)
      emit(it)
    }
  }
}