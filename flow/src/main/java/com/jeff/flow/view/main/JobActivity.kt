package com.jeff.flow.view.main

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.jeff.common.LinearActivity
import com.log.JFLog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class JobActivity : LinearActivity() {

  private var aJob: Job? = null
  private val aChannel = BroadcastChannel<Unit>(Channel.BUFFERED)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    title = this::class.java.simpleName

    initBtn("Job State") {
      JFLog.d("isActive: ${aJob?.isActive}")
      JFLog.d("isCancelled: ${aJob?.isCancelled}")
      JFLog.d("isCompleted: ${aJob?.isCompleted}")
    }

    initBtn("Emit") {
      aJob = lifecycleScope.launch(Dispatchers.IO) {
        flow {
          emit(0)
          delay(3000)
          emit(1)
        }.collect {
          JFLog.d("$it")
        }
      }
    }

    initBtn("CollectLatest") {
      aJob = lifecycleScope.launch(Dispatchers.IO) {
        aChannel.asFlow()
          .collectLatest {
            JFLog.d("$it")
          }
      }
    }

    initBtn("Cancel job") {
      aJob?.cancel()
    }
  }
}