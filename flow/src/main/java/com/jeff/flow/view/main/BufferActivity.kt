package com.jeff.flow.view.main

import android.os.Bundle
import com.jeff.common.LinearActivity
import com.log.JFLog
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.coroutines.CoroutineContext
import kotlin.system.measureTimeMillis

class BufferActivity : LinearActivity(), CoroutineScope {

  private val job by lazy { SupervisorJob() }
  override val coroutineContext: CoroutineContext
    get() = job + Dispatchers.IO

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    title = this::class.java.simpleName

    initBtn("Btn1") {
      launch {
        val time = measureTimeMillis {
          foo()
            .collect { value ->
              delay(500) // 假裝花費 500 毫秒來處理它
              JFLog.d("Collect: $value")
            }
        }
        JFLog.w("Collected in $time ms")
      }
    }

    initBtn("Buffer") {
      launch {
        val time = measureTimeMillis {
          foo()
            .buffer() // 緩衝發射項，無需等待
            .collect { value ->
              delay(500) // 假裝花費 500 毫秒來處理它
              JFLog.d("Collect: $value")
            }
        }
        JFLog.w("Collected in $time ms")
      }
    }

    /**
     * 雖然第一個數字仍在處理中，但第二個和第三個數字已經產生，因此第二個是 conflated ，只有最新的（第三個）被交付給收集器
     */
    initBtn("Conflate") {
      launch {
        val time = measureTimeMillis {
          foo()
            .conflate() // 合並發射項，不對每個值進行處理
            .collect { value ->
              delay(500) // 假裝花費 500 毫秒來處理它
              JFLog.d("Collect: $value")
            }
        }
        JFLog.w("Collected in $time ms")
      }
    }

    initBtn("Latest") {
      launch {
        val time = measureTimeMillis {
          foo()
            .collectLatest { value -> // 取消並重新發射最後一個值
              delay(500) // 假裝花費 500 毫秒來處理它
              JFLog.d("Collect: $value")
            }
        }
        JFLog.w("Collected in $time ms")
      }
    }
  }

  override fun onPause() {
    super.onPause()

    job.cancelChildren()
  }

  override fun onDestroy() {
    super.onDestroy()

    job.cancel("onDestroy")
  }

  fun foo(): Flow<Int> = flow {
    for (i in 1..3) {
      JFLog.i("Send $i")
      delay(100) // 假裝異步等待了 100 毫秒
      emit(i) // 發射下一個值
    }
  }
}