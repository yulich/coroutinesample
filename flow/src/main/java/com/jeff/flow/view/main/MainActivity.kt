package com.jeff.flow.view.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import androidx.activity.viewModels
import androidx.lifecycle.*
import com.jeff.common.LinearActivity
import com.jeff.common.printCurrentThread
import com.jeff.flow.R
import com.jeff.flow.samples.TryFlow
import com.log.JFLog
import com.utils.extension.throttleFirstAsFlow
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*

// https://ricardocosteira.com/going-with-the-flow-rxjava-to-coroutines-part-1
// withContext, cancel https://juejin.im/post/5b5fbc21f265da0f9628a04e
// Android https://medium.com/jastzeonic/kotlin-coroutine-%E9%82%A3%E4%B8%80%E5%85%A9%E4%BB%B6%E4%BA%8B%E6%83%85-685e02761ae0
// https://zhuanlan.zhihu.com/p/79224185
// https://play.kotlinlang.org/hands-on/Introduction%20to%20Coroutines%20and%20Channels/01_Introduction

@ExperimentalCoroutinesApi
class MainActivity : LinearActivity() {

  private val viewModel by viewModels<MainViewModel>()

  private val logHierarchy = 2

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    lifecycleScope.launchWhenCreated {
      JFLog.d("[launchWhenCreated]: ")
    }

    lifecycleScope.launchWhenStarted {
      JFLog.d("[launchWhenStarted]: ")
    }

    lifecycleScope.launchWhenResumed {
      JFLog.d("[launchWhenResumed]: ")
      initBtn1()
      initBtn2()
      initBtn3()
      initBtn4()
      initBtn5()
      initBtn6()
      initBtn7()
      initBtn8()
      initBtn9()
      initBtn10()
      initBtn11()
      initBtn12()
      initBtn13()
      initBtn14()
    }
  }

  override fun onStart() {
    JFLog.d("[onStart]: ")
    super.onStart()
  }

  override fun onResume() {
    JFLog.d("[onResume]: ")
    super.onResume()
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    JFLog.d("[onCreateOptionsMenu:] ")
    menuInflater.inflate(R.menu.menu_main, menu)
    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.action_buffer -> BufferActivity::class
      R.id.action_combine -> CombineActivity::class
      R.id.action_composing -> ComposingActivity::class
      R.id.action_flatteningFlows -> FlatteningFlowsActivity::class
      R.id.action_job -> JobActivity::class
      else -> null
    }?.also {
      startActivity(Intent(this, it.java))
    }

    return super.onOptionsItemSelected(item)
  }

  private fun initBtn1() {
    initBtn("ViewModel fun") {
      //viewModel.fun1()
      //viewModel.fun2()
      //viewModel.fun3()
      //viewModel.fun4_1()
      //viewModel.fun4_2()
      //viewModel.fun5_cancel()
      //viewModel.fun6()
      //viewModel.fun7()
      //viewModel.fun8_retry()
      //viewModel.fun8_retryWhen()
    }
  }

  private fun initBtn2() {
    // take: 限制發送次數
    initBtn("Flow take") {
      viewModel.viewModelScope.launch {
        TryFlow.flow1()
          .take(2)
          .collect {
            JFLog.d(it)
          }
      }
    }
  }

  private fun initBtn3() {
    // takeWhile: 設定中止條件, true: 通過, false: 中止
    initBtn("Flow takeWhile") {
      viewModel.viewModelScope.launch {
        TryFlow.flow1()
          .takeWhile {
            it != "E"
          }
          .collect {
            JFLog.d(it)
          }
      }
    }
  }

  private fun initBtn4() {
    // flow 類似 Rx, 可以透過操作符控制流程.
    initBtn("Flow flowOn") { btn ->
      //runBlocking {
      //CoroutineScope(Dispatchers.IO).launch {
      viewModel.viewModelScope.launch {
        printCurrentThread("viewModelScope", hierarchy = logHierarchy)

        var result = 0
        TryFlow.flowSingle(result)
          .flowOn(Dispatchers.IO)
          .onStart {
            printCurrentThread("Start 1", hierarchy = logHierarchy)
          }
          .onCompletion {
            printCurrentThread("Completion 1", hierarchy = logHierarchy)
          }
          .collect {
            printCurrentThread("collect 1", hierarchy = logHierarchy)
            result = it

            viewModel.viewModelScope.launch {
              btn.text = "JEFF"
            }
          }
        JFLog.d("1: $result")

        TryFlow.flowSingle(result)
          .onStart {
            printCurrentThread("Start 2", hierarchy = logHierarchy)
          }
          .flowOn(Dispatchers.IO)
          .onCompletion {
            printCurrentThread("Completion 2", hierarchy = logHierarchy)
          }
          .collect {
            printCurrentThread("collect 2", hierarchy = logHierarchy)
            result = it
          }
        JFLog.d("2: $result")

        TryFlow.flowSingle(result)
          .onStart {
            printCurrentThread("Start 3", hierarchy = logHierarchy)
          }
          .onCompletion {
            printCurrentThread("Completion 3", hierarchy = logHierarchy)
          }
          .flowOn(Dispatchers.IO)
          .collect {
            printCurrentThread("collect 3", hierarchy = logHierarchy)
            result = it
          }
        JFLog.d("3: $result")
      }
    }

  }

  private fun initBtn5() {
    // withContext 等待第一件事做完, 才會做第二件事.
    initBtn("WithContext") { btn ->
      CoroutineScope(Dispatchers.IO).launch {
        val r1 = withContext(Dispatchers.IO) {
          delay(1000L * 5)
          JFLog.d("Finish 1")
          1
        }

        val r2 = withContext(Dispatchers.IO) {
          JFLog.d("Finish 2")
          10
        }

        JFLog.d("Result: ${r1 + r2}")

        withContext(Dispatchers.Main) {
          btn.text = "${r1 + r2}"
        }
      }
    }
  }

  private fun initBtn6() {
    // CoroutineScope 與 flow 相比只能取得結果, 無法控制流程.
    initBtn("CoroutineScope & Flow") { btn ->
      viewModel.viewModelScope.launch {
        btn.text = TryFlow.withContextSingle()
      }
    }
  }

  private fun initBtn7() {
    // 每個 Flow 都用自己的 launch 或 async 才能同時執行 (async 可以回傳資料)
    initBtn("Flow launch or async 1") {
      //runBlocking {
      viewModel.viewModelScope.launch {
        val r1 = async {
          var result = "A"
          TryFlow.flowSingle(result)
            .onStart {
              printCurrentThread("Start 1", hierarchy = logHierarchy)
            }
            .onCompletion {
              printCurrentThread("Completion 1", hierarchy = logHierarchy)
            }
            .flowOn(Dispatchers.IO)
            .collect {
              printCurrentThread("collect 1", hierarchy = logHierarchy)
              result = it
            }
          result
        }

        val r2 = async {
          var result = "-"
          TryFlow.flowSingle(result)
            .onStart {
              printCurrentThread("Start 2", hierarchy = logHierarchy)
            }
            .onCompletion {
              printCurrentThread("Completion 2", hierarchy = logHierarchy)
            }
            .flowOn(Dispatchers.IO)
            .collect {
              printCurrentThread("collect 2", hierarchy = logHierarchy)
              result = it
            }
          result
        }

        val r3 = async {
          var result = "Z"
          TryFlow.flowSingle(result)
            .onStart {
              printCurrentThread("Start 3", hierarchy = logHierarchy)
            }
            .onCompletion {
              printCurrentThread("Completion 3", hierarchy = logHierarchy)
            }
            .flowOn(Dispatchers.IO)
            .collect {
              printCurrentThread("collect 3", hierarchy = logHierarchy)
              result = it
            }
          result
        }

        JFLog.d("Result (await): ${r1.await() + r2.await() + r3.await()}")

        // getCompleted 必須等到做完才能執行, 否則會crash.
        JFLog.d("Result (completed): ${r1.getCompleted() + r2.getCompleted() + r3.getCompleted()}")
      }
    }
  }

  private fun initBtn8() {
    // 每個 Flow 都用自己的 launch 或 async 才能同時執行
    initBtn("Flow launch or async 2") { btn ->
      //runBlocking {
      viewModel.viewModelScope.launch {
        var result = ""
        val roootJob = launch {
          val job1 = launch {
            TryFlow.flowSingle("1")
              .onStart {
                printCurrentThread("Start 1", hierarchy = logHierarchy)
              }
              .onCompletion {
                printCurrentThread("Completion 1", hierarchy = logHierarchy)
              }
              .collect {
                printCurrentThread("collect 1", hierarchy = logHierarchy)
                result = "$result$it"
              }
          }

          val job2 = launch {
            TryFlow.flowSingle("2")
              .onStart {
                printCurrentThread("Start 2", hierarchy = logHierarchy)
              }
              .onCompletion {
                printCurrentThread("Completion 2", hierarchy = logHierarchy)
              }
              .collect {
                printCurrentThread("collect 2", hierarchy = logHierarchy)
                result = "$result$it"
              }
          }

          val job3 = launch {
            TryFlow.flowSingle("3")
              .onStart {
                printCurrentThread("Start 3", hierarchy = logHierarchy)
              }
              .onCompletion {
                printCurrentThread("Completion 3", hierarchy = logHierarchy)
              }
              .collect {
                printCurrentThread("collect 3", hierarchy = logHierarchy)
                result = "$result$it"
              }
          }
        }

        roootJob.join()

        JFLog.d("Final: $result")
        btn.text = result
      }
    }
  }

  private fun initBtn9() {
    // Flow as LiveData
    initBtn("Flow as LiveData") { btn ->
      TryFlow.flowAsLiveData().observe(this, Observer {
        (btn as Button).text = it
      })
    }
  }

  private fun initBtn10() {
    initBtn("Single thread") {
      TryFlow.tryInnerSingleThread()
    }
  }

  private fun initBtn11() {
    // callbackFlow 與 channelFlow 行為相同, 使用時機看用途. callbackFlow用於callback.
    val btn = initBtn("Callback flow")
    val btn11CallbackFlow = callbackFlow<String> {
      btn.setOnClickListener {
        JFLog.d("Btn11: offer")
        offer("Btn11")
      }
      awaitClose {
        JFLog.w("Btn11: awaitClose")
        btn.setOnClickListener(null)
      }
    }

    lifecycleScope.launch {
      btn11CallbackFlow.collect {
        JFLog.d("Btn11: collect")
        btn.text = it
      }
    }
  }

  private fun initBtn12() {
    val btn = initBtn("Channel")
    val channel = Channel<String>()

    btn.setOnClickListener {
      lifecycleScope.launch {
        channel.send("Btn12")
      }
    }

    lifecycleScope.launch {
      // Only one times
      btn.text = channel.receive()
    }
  }

  private fun initBtn13() {
    val btn = initBtn("Channel consumeAsFlow")
    val channel = Channel<String>()

    var count = 0
    btn.setOnClickListener {
      lifecycleScope.launch {
        count += 1
        channel.send("Btn13: $count")
        JFLog.d("Btn13: Send $count")
      }
    }

    lifecycleScope.launch {
      // Debounce 重新計算
      // Sample 等同於 throttleLast
      channel.throttleFirstAsFlow(3000L).collect {
        JFLog.w("Btn13: Rec $it")
        btn.text = it
      }
    }
  }

  private fun initBtn14() {
    /*
      https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/-mutable-shared-flow.html

      replay: 重播的緩衝
      extraBufferCapacity: 額外的緩衝
      onBufferOverflow: 當訂閱者還未接收的資料超出緩衝時的策略.
     */

    val btn14Flow = MutableSharedFlow<Int>(
      replay = 0,
      extraBufferCapacity = 1,
      onBufferOverflow = BufferOverflow.DROP_OLDEST,
    )

    lifecycleScope.launch {
      lifecycle.repeatOnLifecycle(Lifecycle.State.RESUMED) {
        JFLog.d("Btn14 collect when resumed")
        btn14Flow.collect {
          JFLog.d("Btn14: Rec $it")
        }
      }
    }

    var count = 0
    initBtn("SharedFlow") {
      btn14Flow.tryEmit(count++)
    }
  }
}
