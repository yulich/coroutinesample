package com.jeff.flow.samples

import androidx.lifecycle.asLiveData
import com.jeff.common.printCurrentThread
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart

object TryFlow {

    fun flow1() = flow {
        listOf("A", "B", "C", "D", "E", "F", "G").forEach { send ->
            emit(send)
        }
    }

    fun flowSingle(c: Int) = flow {
        printCurrentThread(hierarchy = 3)
        delay(5000L)
        emit(c + 10)
    }

    fun flowSingle(string: String) = flow {
        printCurrentThread(hierarchy = 3)
        delay(5000L)
        emit(string)
    }

    suspend fun withContextSingle() = withContext(Dispatchers.IO) {
        printCurrentThread(hierarchy = 3)
        delay(3000L)
        "Testing"
    }

    fun flowAsLiveData() = flow {
        printCurrentThread(hierarchy = 3)
        delay(3000L)
        emit("Live 1")
        delay(3000L)
        emit("Live 2")
        delay(3000L)
        emit("Live 3")
    }
        .flowOn(Dispatchers.IO) // 以上用指定thread, 以下用原本thread
        .onStart {
            printCurrentThread("flowAsLiveData: onStart", hierarchy = 2)
        }
        .onCompletion {
            printCurrentThread("flowAsLiveData: onCompletion", hierarchy = 2)
        }
        .asLiveData()

    fun tryInnerSingleThread() {
        newSingleThreadContext("Ctx1").use { ctx1 ->
            newSingleThreadContext("Ctx2").use { ctx2 ->
                runBlocking(ctx1) {
                    printCurrentThread("Run ctx1", hierarchy = 2)

                    withContext(ctx2) {
                        printCurrentThread("Work in ctx2", hierarchy = 2)
                    }

                    printCurrentThread("Back to ctx2", hierarchy = 2)
                }
            }
        }
    }
}