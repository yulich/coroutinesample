package com.jeff.flow.samples

import com.log.JFLog
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

object BasicSample {
    // List<T>, listOf
    fun foo1(): List<Int> = listOf(1, 2, 3)

    // Sequence<T>, sequence
    fun foo2(): Sequence<Int> = sequence {
        for (i in 1..3) {
            Thread.sleep(2000)
            yield(i) // 產生下一個值
        }
    }

    // suspend, List<T>, listOf
    suspend fun foo3(): List<Int> {
        delay(2000)
        return listOf(1, 2, 3)
    }

    // Flow<T>, flow
    fun foo4(): Flow<Int> = flow {
        JFLog.i("Flow start")
        for (i in 1..3) {
            delay(500)
            emit(i) // 發送下一個值
        }
        JFLog.i("Flow end")
    }

    // Flow<T>, flow
    fun foo5(): Flow<Int> = flow {
        try {
            JFLog.i("Flow start")
            for (i in 1..3) {
                delay(500)
                emit(i) // 發送下一個值
            }
        } catch (e: Exception) {
            throw e
        } finally {
            JFLog.i("Flow end")
        }
    }
}