package com.jeff.supervision

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.log.JFLog
import kotlinx.coroutines.*

/**
 * https://kotlinlang.org/docs/exception-handling.html#supervision
 */

class MainActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    /**
     * Job: child Job Exception 會影響到 parent Job
     * SupervisorJob: child Job 出現 Exception, 不會影響到 parent Job.
     */

    testJob()
//    testSupervisorJob()
//    testSupervisorScopeWithJob()
  }

  private fun testJob() {
    runBlocking {
      val job = Job()
      with(CoroutineScope(coroutineContext + job)) {
        // launch the first child -- its exception is ignored for this example (don't do this in practice!)
        val firstChild = launch(CoroutineExceptionHandler { _, _ -> }) {
          delay(500L)
          JFLog.d("The first child is failing")
          throw AssertionError("The first child is cancelled")
        }
        // launch the second child
        val secondChild = launch {
          firstChild.join()
          // Cancellation of the first child is not propagated to the second child
          JFLog.d("The first child is cancelled: ${firstChild.isCancelled}, but the second one is still active")
          try {
            delay(Long.MAX_VALUE)
          } finally {
            // But cancellation of the supervisor is propagated
            JFLog.d("The second child is cancelled because the supervisor was cancelled")
          }
        }
        // wait until the first child fails & completes
        firstChild.join()
        delay(3_000L)
        JFLog.d("Cancelling the supervisor")
        job.cancel()
        secondChild.join()
      }
    }
  }

  private fun testSupervisorJob() {
    runBlocking {
      val supervisor = SupervisorJob()
      with(CoroutineScope(coroutineContext + supervisor)) {
        // launch the first child -- its exception is ignored for this example (don't do this in practice!)
        val firstChild = launch(CoroutineExceptionHandler { _, _ -> }) {
          delay(500L)
          JFLog.d("The first child is failing")
          throw AssertionError("The first child is cancelled")
        }
        // launch the second child
        val secondChild = launch {
          firstChild.join()
          // Cancellation of the first child is not propagated to the second child
          JFLog.d("The first child is cancelled: ${firstChild.isCancelled}, but the second one is still active")
          try {
            delay(Long.MAX_VALUE)
          } finally {
            // But cancellation of the supervisor is propagated
            JFLog.d("The second child is cancelled because the supervisor was cancelled")
          }
        }
        // wait until the first child fails & completes
        firstChild.join()
        delay(3_000L)
        JFLog.d("Cancelling the supervisor")
        supervisor.cancel()
        secondChild.join()
      }
    }
  }

  private fun testSupervisorScopeWithJob() {
    runBlocking {
      val job = Job()
      val scope = CoroutineScope(coroutineContext + job)
      scope.launch {
        supervisorScope {
          // launch the first child -- its exception is ignored for this example (don't do this in practice!)
          val firstChild = launch(CoroutineExceptionHandler { _, _ -> }) {
            delay(500L)
            JFLog.d("The first child is failing")
            throw AssertionError("The first child is cancelled")
          }
          // launch the second child
          val secondChild = launch {
            firstChild.join()
            // Cancellation of the first child is not propagated to the second child
            JFLog.d("The first child is cancelled: ${firstChild.isCancelled}, but the second one is still active")
            try {
              delay(Long.MAX_VALUE)
            } finally {
              // But cancellation of the supervisor is propagated
              JFLog.d("The second child is cancelled because the supervisor was cancelled")
            }
          }
          // wait until the first child fails & completes
          firstChild.join()
          delay(3_000L)
          JFLog.d("Cancelling the supervisor")
          this.cancel()
          secondChild.join()
        }
      }
    }
  }
}