package com.jeff.coroutinesample

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.log.JFLog
import kotlinx.coroutines.*

class CoroutineActivity2 : AppCompatActivity(), CoroutineScope by MainScope() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_coroutine)

    val btnStart = findViewById<Button>(R.id.btn1)
    btnStart.setOnClickListener {
      doSomething(this)
    }

    val btnCancel = findViewById<Button>(R.id.btn2)
    btnCancel.setOnClickListener {
      this.cancel("Cancel onClicked")
    }
  }

  private fun doSomething(scope: CoroutineScope) {
    scope.launch {
      var isDone = false

      try {
        repeat(20) { i ->
          delay(500L)
          JFLog.d("Coroutine $i is done")
        }

        isDone = true
      } catch (e: Exception) {
        JFLog.w("catch: ${e.message}")
      } finally {
        if (isDone) {
          JFLog.w("Done...")
        } else {
          JFLog.w("Finally...")
        }
      }
    }
  }

  override fun onDestroy() {
    super.onDestroy()

    cancel("Cancel onDestroy")
  }
}