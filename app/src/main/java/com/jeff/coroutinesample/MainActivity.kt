package com.jeff.coroutinesample

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.jeff.common.LinearActivity
import com.jeff.coroutinesample.samples.*
import kotlinx.coroutines.launch

// 介紹, https://medium.com/jastzeonic/kotlin-coroutine-%E9%82%A3%E4%B8%80%E5%85%A9%E4%BB%B6%E4%BA%8B%E6%83%85-685e02761ae0
// 取消與超時, http://www.liying-cn.net/kotlin/docs/reference/coroutines/cancellation-and-timeouts.html
class MainActivity : LinearActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initBtn("BasicSample fun1") {
            BasicSample.fun1()
        }

        initBtn("BasicSample fun2") {
            BasicSample.fun2()
        }

        initBtn("BasicSample fun3") {
            BasicSample.fun3()
        }

        initBtn("BasicSample fun4") {
            BasicSample.fun4()
        }

        initBtn("BasicSample fun5") {
            BasicSample.fun5()
        }

        initBtn("BasicSample repeat") {
            BasicSample.repeat()
        }

        initBtn("BasicSample all") {
            BasicSample.all()
        }

        initBtn("Test scope") {
            ScopeSample.testScope()
        }

        initBtn("CancelAndTimeoutSample fun1") {
            CancelAndTimeoutSample.fun1()
        }

        initBtn("CancelAndTimeoutSample fun2") {
            CancelAndTimeoutSample.fun2()
        }

        initBtn("CancelAndTimeoutSample tryFinally") {
            CancelAndTimeoutSample.tryFinally()
        }

        initBtn("CancelAndTimeoutSample timeout") {
            CancelAndTimeoutSample.timeout()
        }

        initBtn("SuspendingSample withoutAsync") {
            SuspendingSample.withoutAsync()
        }

        initBtn("SuspendingSample testAsync") {
            SuspendingSample.testAsync()
        }

        initBtn("SuspendingSample lazyStartAsync") {
            SuspendingSample.lazyStartAsync()
        }

        initBtn("SuspendingSample combineFunc") {
            SuspendingSample.combineFunc()
        }

        initBtn("SuspendingSample testFailed") {
            SuspendingSample.testFailed()
        }

        initBtn("HandlerException") {
            ExceptionSample.handlerException()
        }

        initBtn("Ticker") {
            lifecycleScope.launch {
                TickerSample.testTicker()
            }
        }
    }
}
