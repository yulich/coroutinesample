package com.jeff.coroutinesample.samples

import com.jeff.common.printCurrentThread
import com.log.JFLog
import kotlinx.coroutines.*

// http://www.liying-cn.net/kotlin/docs/reference/coroutines/basics.html

object BasicSample {

    // 協程的 join & await 比較:
    // join: 單純等待完成, await: 等待完成, 有回傳值.

    fun fun1() {
        JFLog.w("Start fun1...")

        GlobalScope.launch {
            JFLog.d("Start GlobalScope...")
            delay(3000L)
            JFLog.d("End GlobalScope...")
        }

        // 此表達式會在主線程運行
        runBlocking {
            JFLog.i("Start blocking...")
            delay(3000L)
            JFLog.i("End blocking...")
        }

        JFLog.w("End fun1...")
    }

    fun fun2() {
        JFLog.w("Start fun2...")
        runBlocking {
            JFLog.d("Start blocking")

            // 在 runBlocking 範圍內啟動新的協程
            launch {
                JFLog.i("Start launch")
                delay(3000L)
                JFLog.i("End launch")
            }

            JFLog.d("End blocking")
        }

        JFLog.w("End fun2...")
    }

    fun fun3() {
        GlobalScope.launch {
            JFLog.w("Start fun3...")

            val job = launch {
                JFLog.d("Start job")
                delay(3000L)
                JFLog.d("End job")
            }

            JFLog.i("Before join")
            job.join()
            JFLog.i("After join")

            JFLog.w("End fun3...")
        }
    }

    fun fun4() {
        // 在同一階層, launch 會延遲執行.

        val logHierarchy = 3
        runBlocking {
            launch {
                JFLog.i("1...Start")
                printCurrentThread(hierarchy = logHierarchy)
                delay(6000L)
                JFLog.w("1...End")
            }

            // 創建新的協程作用範圍
            coroutineScope {
                launch {
                    JFLog.i("2...Start")
                    printCurrentThread(hierarchy = logHierarchy)
                    delay(4000L)
                    JFLog.w("2...End")
                }

                JFLog.i("3...Start")
                printCurrentThread(hierarchy = logHierarchy)
                delay(2000L)
                JFLog.w("3...End")
            }

            // 與1同一層, 不需要等到1結束.
            // 但要等到 coroutineScope 運行結束後
            JFLog.i("4...Start")
            printCurrentThread(hierarchy = logHierarchy)
            JFLog.w("4...End")
        }
    }

    ////////////////////////////////////////////////////////////

    fun fun5() {
        runBlocking {
            fun5_1()
            fun5_2()
        }
    }

    // 等同於launch {...} 中的代碼抽出來定義為一個獨立的函數, 必須在scope中在能呼叫.
    private suspend fun fun5_1() {
        JFLog.d("Start suspend")
        delay(2000L)
        JFLog.d("End suspend")
    }

    private suspend fun fun5_2() {
        JFLog.d("Start suspend")
        delay(1000L)
        JFLog.d("End suspend")
    }

    ////////////////////////////////////////////////////////////

    fun repeat() {
        GlobalScope.launch {
            repeat(100) { i ->
                JFLog.d("I: $i")
                delay(100)
            }
        }
    }

    ////////////////////////////////////////////////////////////

    fun all() {
        JFLog.d("Init job")

        runBlocking {
            val job = GlobalScope.launch {
                repeat(5) { i ->
                    launch {
                        delay(3000L)
                        JFLog.d("Repeat: $i")
                    }
                }
            }

            job.join() // 等待所有協程完成
            JFLog.d("Job done")
        }
    }
}