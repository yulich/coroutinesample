package com.jeff.coroutinesample.samples

import com.log.JFLog
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

// http://www.liying-cn.net/kotlin/docs/reference/coroutines/composing-suspending-functions.html

object SuspendingSample {

    // 同步
    fun withoutAsync() {
        runBlocking {
            val t = measureTimeMillis {
                val first = myName()
                val second = myNumber()

                JFLog.d("${first + second}")
            }

            JFLog.d("Completed in $t ms")
        }
    }

    /**
     * 概念上來說, async 就好象 launch 一樣. 它啟動一個獨立的協程, 也就是一個輕量的線程, 與其他所有協程同時執行.
     * 區別在於, launch 返回一個 Job, 其中不帶有結果值, 而 async 返回一個 Deferred – 一個輕量的, 非阻塞的 future, 代表一個未來某個時刻可以得到的結果值.
     * 你可以對一個延期值(deferred value)使用 .await() 來得到它最終的計算結果, 但 Deferred 同時也是一個 Job, 因此如果需要的話, 你可以取消它.
     */
    fun testAsync() {
        runBlocking {
            val t = measureTimeMillis {
                val first = async {
                    myName()
                }

                val second = async {
                    myNumber()
                }

                JFLog.d("${first.await() + second.await()}")
            }

            JFLog.d("Completed in $t ms")
        }
    }

    // 延遲啟動
    fun lazyStartAsync() {
        runBlocking {
            val t = measureTimeMillis {
                val first = async(start = CoroutineStart.LAZY) {
                    myName()
                }

                val second = async(start = CoroutineStart.LAZY) {
                    myNumber()
                }

                first.start() // 啟動協程
                second.start() // 啟動協程

                JFLog.d("${first.await() + second.await()}")
            }

            JFLog.d("Completed in $t ms")
        }
    }

    fun combineFunc() {
        runBlocking {
            val t = measureTimeMillis {
                JFLog.d(combineAsync())
            }

            JFLog.d("Completed in $t ms")
        }
    }

    private suspend fun myName(): String {
        delay(2000L)
        return "Jeff"
    }

    private suspend fun myNumber(): Int {
        delay(2000L)
        return 17
    }

    private suspend fun combineAsync(): String = coroutineScope {
        val first = async {
            myName()
        }

        val second = async {
            myNumber()
        }

        "${first.await()}${second.await()}"
    }

    private fun getNameAsync() = GlobalScope.async {
        myName()
    }

    private fun getNumberAsync() = GlobalScope.async {
        myNumber()
    }

    ////////////////////////////////////////////////////////////

    fun testFailed() {
        GlobalScope.launch {
            try {
                async {
                    // 不同協程不能夠共用try-catch
                    try {
                        failedFunc()
                    } catch (e: Exception) {
                        JFLog.e(e)
                    }
                }
                failedFunc()
            } catch (e: Exception) {
                JFLog.e(e)
            }
        }
    }

    // 其中一個協程執行失敗, 作用範圍內其他協程也會被取消.
    private suspend fun failedFunc(): Int = coroutineScope {

        val first = async(CoroutineName("FirstCoroutine")) {
            try {
                JFLog.w("Execute first fun")
                delay(10000L)
                101
            } finally {
                JFLog.w("First func was cancelled")
            }
        }

        val second = async<Int>(CoroutineName("SecondCoroutine")) {
            JFLog.w("Execute second fun")
            throw Exception("Thread name = ${Thread.currentThread().name}")
        }

        var result = 0

        JFLog.d("First await")
        result += first.await()

        // Second 意外取消, 後面來不及 first 完成就結束了.
        JFLog.d("Second await")

        result += second.await()

        JFLog.d("Finish")

        result
    }
}