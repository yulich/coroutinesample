package com.jeff.coroutinesample.samples

import com.log.JFLog
import kotlinx.coroutines.*

// http://www.liying-cn.net/kotlin/docs/reference/coroutines/exception-handling.html

object ExceptionSample {

    /**
     * 如果一個協程遇到了 CancellationException 以外的異常, 那麼它會使用這個異常來取消自己的父協程.
     * 這種行為不能覆蓋, 而且 Kotlin 使用這個機製來實現結構化, 而不是依賴於 CoroutineExceptionHandler 的實現.
     * 當所有的子協程全部結束後, 原始的異常會被父協程處理.
     * 這也是為什麼, 在這些示例程序中, 總是在 GlobalScope 內創建的協程上安裝 CoroutineExceptionHandler.
     * 如果在 main runBlocking 的作用範圍內啟動的協程上安裝異常處理器, 是毫無意義的,
     * 因為子協程由於異常而終止後之後, 主協程一定會被取消, 而忽略子協程安裝的異常處理器.
     */
    fun handlerException() {
        runBlocking {
            val handler = CoroutineExceptionHandler { _, exception ->
                JFLog.e("發生錯誤: $exception")
                JFLog.e("Suppressed: ${exception.suppressed.contentToString()}")
            }

            GlobalScope.launch(handler) {
                repeat(5) { i ->
                    launch {
                        inner(i)
                    }
                }

                launch {
                    delay(1000L)
                    JFLog.d("Throw exception...")
                    throw Exception("Global Scope")
                }
            }
        }
    }

    private suspend fun inner(i: Int) {
        var isDone = false
        try {
            delay(Long.MAX_VALUE)
            isDone = true
        } finally {
            if (isDone) {
                JFLog.w("Done $i...")
            } else {
                JFLog.w("Cancelled $i...")
                throw Exception("Error $i")
            }
        }
    }
}