package com.jeff.coroutinesample.samples

import com.log.JFLog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

// http://www.liying-cn.net/kotlin/docs/reference/coroutines/coroutine-context-and-dispatchers.html

object ScopeSample {

    val mainScpoe = CoroutineScope(Dispatchers.Main)

    val ioScope = CoroutineScope(Dispatchers.IO)

    val defaultScope = CoroutineScope(Dispatchers.Default)

    // 非受限 -- 將會在主線程中執行
    val unconfinedScpoe = CoroutineScope(Dispatchers.Unconfined)

    fun testScope() {
        ioScope.launch {
            JFLog.d("${Thread.currentThread().name}")

            mainScpoe.launch {
                JFLog.d("${Thread.currentThread().name}")

                defaultScope.launch {
                    JFLog.d("${Thread.currentThread().name}")
                }

                unconfinedScpoe.launch {
                    JFLog.d("${Thread.currentThread().name}")
                }
            }
        }
    }
}