package com.jeff.coroutinesample.samples

import com.jeff.common.printCurrentThread
import kotlinx.coroutines.channels.ticker

object TickerSample {
  suspend fun testTicker() {
    val ticker = ticker(delayMillis = 6_000L, initialDelayMillis = 1_000L)
    for (event in ticker) {
      printCurrentThread("Ticker", hierarchy = 2)
    }
  }
}