package com.jeff.coroutinesample.samples

import com.log.JFLog
import kotlinx.coroutines.*

// http://www.liying-cn.net/kotlin/docs/reference/coroutines/cancellation-and-timeouts.html

object CancelAndTimeoutSample {

    fun fun1() {
        runBlocking {
            JFLog.w("Start fun1")
            val job = launch {
                repeat(1000) {
                    JFLog.d("T: $it")
                    delay(300L)
                }

                JFLog.i("Finish repeat")
            }

            delay(2000L)

            JFLog.d("I'm tired of waiting!")
            job.cancel() // 取消
            job.join() // 等待
            JFLog.w("Finish fun1")
        }
    }

    fun fun2() {
        runBlocking {
            JFLog.w("Start fun2")
            val job = launch {
                var count = 0
                while (count < 20) {
                    JFLog.d("T: $count")
                    count++
                    delay(300L)
                }

                JFLog.i("Finish repeat")
            }

            delay(2000L)

            JFLog.d("I'm tired of waiting!")
            job.cancelAndJoin()
            JFLog.w("Finish fun2")
        }
    }

    fun tryFinally() {
        runBlocking {
            JFLog.w("Start fun2")
            val job = launch {
                try {
                    var count = 0
                    while (count < 20) {
                        JFLog.d("T: $count")
                        count++
                        delay(300L)
                    }

                    JFLog.i("Finish repeat")
                } finally {
                    JFLog.i("Finally...")
                }
            }

            delay(2000L)

            JFLog.d("I'm tired of waiting!")
            job.cancelAndJoin()
            JFLog.w("Finish fun2")
        }
    }

    fun timeout() {
        runBlocking {
            // timeout 會 exception
            val r = withTimeoutOrNull(1000L) {
                try {
                    var count = 0

                    while (true) {
                        JFLog.d("T: ${count++}")
                        delay(300L)
                    }

                    JFLog.i("Finish repeat")
                    "Done"
                } finally {
                    JFLog.i("Finally...")
                    "Finally"// 沒有作用
                }
            }

            JFLog.w("Result: $r")
        }
    }
}