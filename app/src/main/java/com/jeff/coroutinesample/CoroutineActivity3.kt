package com.jeff.coroutinesample

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.log.JFLog
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

// Follow steps
// Step 1: Add CoroutineScope
class CoroutineActivity3 : AppCompatActivity(), CoroutineScope {

  // Step 2: override val coroutineContext
  override val coroutineContext: CoroutineContext
    get() = Dispatchers.Default + job

  // Step 3 Assignee job
  private val job: Job by lazy { Job() }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_coroutine)

    val btnStart = findViewById<Button>(R.id.btn1)
    btnStart.setOnClickListener {
      doSomething()
    }

    val btnCancel = findViewById<Button>(R.id.btn2)
    btnCancel.setOnClickListener {
      JFLog.d("Jobs: ${job.children.count()}")
      // try-catch 捕捉例外, 不會造成一般 job 死亡, 但 CoroutineExceptionHandler 會.
      try {
        launch {
          throw Exception("Wow!")
        }
      } catch (e: Exception) {
      }
    }
  }

  private fun doSomething() {
    launch {
      var isDone = false

      try {
        repeat(20) { i ->
          delay(500L)
          JFLog.d("Coroutine $i is done")
        }

        isDone = true
      } catch (e: Exception) {
        JFLog.w("catch: ${e.message}")
      } finally {
        if (isDone) {
          JFLog.w("Done...")
        } else {
          JFLog.w("Finally...")
        }
      }
    }
  }

  override fun onDestroy() {
    super.onDestroy()

    // Step 4 Cancel onDestroy
    job.cancel("Cancel onDestroy")
  }
}