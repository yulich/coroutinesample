package com.jeff.common

import android.os.Bundle
import android.view.Gravity
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.jeff.common.databinding.ActivityLinearBinding

abstract class LinearActivity : AppCompatActivity() {

  lateinit var binding: ActivityLinearBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    binding = ActivityLinearBinding.inflate(layoutInflater)
    setContentView(binding.root)
  }

  fun initBtn(text: String, block: ((Button) -> Unit)? = null) =
    Button(this).apply {
      this.text = text
      setOnClickListener {
        block?.invoke(this)
      }

      val lp = LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT,
        LinearLayout.LayoutParams.WRAP_CONTENT
      )

      lp.topMargin = (5 * resources.displayMetrics.density).toInt()
      lp.gravity = Gravity.CENTER
      binding.contentLayout.addView(this, lp)
    }
}