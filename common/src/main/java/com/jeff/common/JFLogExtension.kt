package com.jeff.common

import com.log.JFLog

fun printCurrentThread(tag: String = "", hierarchy: Int = 1) {
  val prefix = if (tag.isBlank()) {
    ""
  } else {
    "$tag: "
  }

  when (Thread.currentThread().name) {
    "main" -> JFLog.i(
      hierarchy = hierarchy,
      message = "${prefix}Thread(${Thread.currentThread().name})"
    )
    else -> JFLog.w(
      hierarchy = hierarchy,
      message = "${prefix}Thread(${Thread.currentThread().name})"
    )
  }
}